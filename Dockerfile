FROM python:latest

RUN pip install --upgrade pip; \
    pip install --no-cache-dir "ansible"; \
    # Ansible
    wget https://raw.githubusercontent.com/ansible-collections/azure/dev/requirements-azure.txt; \
    pip install -r requirements-azure.txt; \
    rm requirements-azure.txt; \
    ansible-galaxy collection install azure.azcollection; \
    # Kubectl
    curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"; \
    chmod +x ./kubectl; \
    mv ./kubectl /usr/local/bin/kubectl; \
    # Flux
    curl -s https://fluxcd.io/install.sh | bash; 
